#include <iostream>
#include <cmath>
#include "cloud.h"

Cloud::Cloud( cocos2d::Layer* p, const std::string& name, int pe, float lx, float ly) : 
  Base( p ),
  sprite( cocos2d::Sprite::create(name) ),
  size( sprite->getContentSize().width, sprite->getContentSize().height ),
  period(pe),
  fd(0)
{
  getParentLayer()->addChild(sprite, 1);
  sprite->setPosition( cocos2d::Point(lx, ly) ); 
}

Cloud::Cloud( cocos2d::Layer* p, const std::string& name, int pe, float lx, float ly, 
  float vx, float vy) : 
  Base( vx, vy, p ),
  sprite( cocos2d::Sprite::create(name) ),
  size( sprite->getContentSize().width, sprite->getContentSize().height ),
  period(pe),
  fd(0)
{
  getParentLayer()->addChild(sprite, 1);
  size = cocos2d::Vec2( sprite->getContentSize().width, sprite->getContentSize().height );
  sprite->setPosition( cocos2d::Point(lx, ly) ); 
}

Cloud::Cloud( cocos2d::Layer* p, const std::string& name, int pe, float lx, float ly, 
  float vx, float vy, float s) : 
  Base( vx, vy, p ),
  sprite( cocos2d::Sprite::create(name) ),
  // size( sprite->getContentSize().width, sprite->getContentSize().height ),
  period(pe),
  fd(0)
{
  getParentLayer()->addChild(sprite, 1);
  sprite->setScale(s);
  size = cocos2d::Vec2( sprite->getContentSize().width, sprite->getContentSize().height );
  sprite->setPosition( cocos2d::Point(lx, ly) ); 
}


void Cloud::update(float dt) {
  cocos2d::Vec2 position = sprite->getPosition();
  cocos2d::Vec2 incr = getVelocity() * dt;
  setshift(incr.x+fd);
  fd = 0;
  sprite->setPosition(position.x + incr.x, position.y + incr.y );

  cocos2d::Point location = sprite->getPosition();

  if ( location.x - getViewSize().width/2 > period * (getViewSize().width/2) + size.x/2 ) {
    sprite->setPosition(location.x - (period + 1) * (getViewSize().width/2), position.y );
  }
  if ( getViewSize().width/2 - location.x > period * (getViewSize().width/2) + size.x/2 ) {
    sprite->setPosition((period + 1) * (getViewSize().width/2) + location.x, position.y );
  }  
  if ( location.y > getViewSize().height + size.x/2 ) {
    sprite->setPosition(position.x, -size.x/2 );
  }  
  if ( location.y < -size.x/2 ) {
    sprite->setPosition(position.x, getViewSize().height + size.x/2 );
  }  


}

float Cloud::getDistance( const Base* b ) const {
  return sprite->getPosition().distanceSquared(b->getPos());
}


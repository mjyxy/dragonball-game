#include <iostream>
#include <cmath>
#include "smart.h"
#include "chunk.h"
#include "gamedata.h"

Smart::Smart(cocos2d::Layer* p, Base* play, const std::string& name, int noFrames, 
  const std::string& name2, int noFrames2, int pe, float lx, float ly, float vy, float ythreshold  ) :
  Enemy( p ,play, name, noFrames, pe, lx,ly ),
  speedy(vy),
  threshold(ythreshold),
  oriy(ly)
{


  std::string plistName = name2 +".plist";
  cocos2d::SpriteFrameCache::
    getInstance()->addSpriteFramesWithFile(plistName.c_str());

  // Set up the frames of animation:
  std::string filename = name2 + "%1d.png";
  cocos2d::Vector<cocos2d::SpriteFrame*>
    frames = getFrames(filename.c_str(), noFrames2);

  sprite2 = cocos2d::Sprite::createWithSpriteFrame(frames.front());
  p->addChild(sprite2, 1); 

  sprite2->setPosition(lx,ly);

  cocos2d::Animation*
  animation = cocos2d::Animation::createWithSpriteFrames(frames, 1.0f/8);
  sprite2->runAction(
    cocos2d::RepeatForever::create(cocos2d::Animate::create(animation))
  );
  sprite2->setVisible(false);

}




void Smart::update(float dt) {

  if( getplayer()->getPos().x < getPos().x ){
      sprite2->setScaleX(-1.0);
      getSprite()->setScaleX(-1.0);

    if( getPos().x - getplayer()->getPos().x   < 300 ){
      if(getPos().y != threshold) VelY(speedy);
    }else{
      if(getPos().y != oriy) VelY(-speedy);
    }

  }else{
    sprite2->setScaleX(1.0);
    getSprite()->setScaleX(1.0);

    if( getplayer()->getPos().x -getPos().x  < 300 ){
      if(getPos().y != threshold) VelY(speedy);
    }else{
      if(getPos().y != oriy) VelY(-speedy);
    }
  }
  
  Enemy::update(dt);
  cocos2d::Point location = sprite2->getPosition();

  if ( location.y > threshold ) {
    getSprite()->setPosition(location.x, threshold );
    VelY(0);
  }  
  if ( location.y < oriy ) {
    getSprite()->setPosition(location.x, oriy );
    VelY(0);
  }  
  if(!isexploded()){
    if(VelY()!=0){
      sprite2->setVisible(true);
      getSprite()->setVisible(false);
    }else{
      sprite2->setVisible(false);
      getSprite()->setVisible(true);  
    }
  }else{
    sprite2->setVisible(false);
    getSprite()->setVisible(false);
  }

  sprite2->setPosition(getPos());

}





#ifndef ENEMY__H
#define ENEMY__H
#include "cocos2d.h"
#include "base.h"
#include <cstring>


class Enemy : public Base {
public:
  Enemy(cocos2d::Layer* p, Base* play, const std::string& name, int noFrames, int pe, float lx, float ly);
  Enemy(cocos2d::Layer* p, Base* play, const std::string& name, int noFrames, int pe, float lx, float ly, 
    float vx, float vy);

  virtual ~Enemy();
  virtual void update(float);
  cocos2d::Sprite* getSprite() const { return sprite; }
  cocos2d::Vector<cocos2d::SpriteFrame*> getFrames(const char *format, int count);
  float getDistance( const Base* b ) const ;
  cocos2d::Vec2 getPos() const { return sprite->getPosition(); }

  void makechunks();
  void resetshowlist();

  cocos2d::Vec2 getSize() { return size; }
  void setfd(float f) { fd = f; }
  Base* getplayer() { return player; }

private:
  std::string myname;
  cocos2d::Sprite* sprite;
  cocos2d::Vec2 size;
  int period;
  Base* player;
  std::list<Base*> showlist;
  std::list<Base*> freelist;

  float fd;
};
#endif

#include <iostream>
#include <cmath>
#include "kame.h"
#include <sstream>

Kame::~Kame() { 
  act1->release(); 
  act2->release(); 

  getParentLayer()->removeChild(sprite1); 
  getParentLayer()->removeChild(sprite2); 

}


Kame::Kame( cocos2d::Layer* p, const std::string& name,
  int noFrames, float lx, float ly, bool facetoright) : 
  Base( p ),
  myname(name),
  num(0),
  done(false),
  currentframe(0),
  size( 0,0 ),
  bo()
{
  std::string plistName = name +".plist";
  cocos2d::SpriteFrameCache::
    getInstance()->addSpriteFramesWithFile(plistName.c_str());

  // Set up the frames of animation:
  std::string filename = name + "%1d.png";
  cocos2d::Vector<cocos2d::SpriteFrame*>
    frames = getFrames(filename.c_str(), noFrames);

  sprite1 = cocos2d::Sprite::createWithSpriteFrame(frames.front());
  p->addChild(sprite1, 1); 


  sprite1->setPosition(lx,ly);

  sprite1->setScaleX(facetoright?1.0:-1.0);

  cocos2d::Animation*
  animation = cocos2d::Animation::createWithSpriteFrames(frames, 1.0f/8);
  act1 = cocos2d::Repeat::create(cocos2d::Animate::create(animation),1);
  act1->retain();
  sprite1->runAction(act1);

  sprite2 = cocos2d::Sprite::createWithSpriteFrame(frames.front());
  p->addChild(sprite2, 1); 


  sprite2->setPosition(lx,ly);

  sprite2->setScaleX(facetoright?1.0:-1.0);
  cocos2d::Vector<cocos2d::SpriteFrame*>
  frames2 = getFrames(filename.c_str(), 5);  // run half
  cocos2d::Animation*
  animation2 = cocos2d::Animation::createWithSpriteFrames(frames2, 1.0f/8);
  act2 = cocos2d::Repeat::create(cocos2d::Animate::create(animation2),1);
  act2->retain();
  sprite2->runAction(act2);

  std::stringstream ss;

  bo.reserve(9);

  for(int i=1;i<10;i++){
    ss << "bo" << i << ".png";
    filename = ss.str();
    bo.push_back(cocos2d::Sprite::create(filename));
    p->addChild(bo[i-1],1);
    bo[i-1]->setVisible(false);
    if(facetoright) bo[i-1]->setPosition(lx + bo[i-1]->getContentSize().width/2 + sprite1->getContentSize().width/2, ly);
    else bo[i-1]->setPosition(lx - bo[i-1]->getContentSize().width/2 - sprite1->getContentSize().width/2, ly);
    ss.str("");
    ss.clear();
  }

}

void Kame::update(float dt) {
  if(act2->isDone()){
    sprite2->setVisible(false);
    num++;
    if(num%3==0){
      if(currentframe<9){
        if(currentframe!=0) bo[currentframe-1]->setVisible(false);
        bo[currentframe++]->setVisible(true);
      }else{
        bo[8]->setVisible(false);
        done =true;
      }
    }
  }
}

float Kame::getDistance( const Base* b ) const {
  return sprite1->getPosition().distanceSquared(b->getPos());
}


// Function getFrames marshals the sprite frames into a cocos
// vector using the format string passed to the function.
cocos2d::Vector<cocos2d::SpriteFrame*> 
Kame::getFrames(const char *format, int count) {
  cocos2d::SpriteFrameCache* 
  spritecache = cocos2d::SpriteFrameCache::getInstance();
  cocos2d::Vector<cocos2d::SpriteFrame*> animFrames;
  char str[100];
  for(int i = 1; i <= count; i++) {
    sprintf(str, format, i);
    animFrames.pushBack(spritecache->getSpriteFrameByName(str));
  }
  return animFrames;
}


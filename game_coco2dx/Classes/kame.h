#ifndef KAME__H
#define KAME__H
#include "cocos2d.h"
#include <string>
#include <list>
#include <vector>
#include <iostream>
#include "base.h"

class Kame : public Base {
public:

  Kame(cocos2d::Layer* p, const std::string& name,
    int noFrames, float lx, float ly, bool facetoright);
  virtual ~Kame();
  virtual void update(float dt);
  cocos2d::Sprite* getSprite() const { return currentframe==9?bo[8]:bo[currentframe]; }
  cocos2d::Vector<cocos2d::SpriteFrame*> getFrames(const char *format, int count);
  float getDistance( const Base* b ) const ;
  cocos2d::Vec2 getPos() const { return currentframe==9?bo[8]->getPosition():bo[currentframe]->getPosition(); }

  cocos2d::Vec2 getSize() { return size; }
  bool isD() { return act1->isDone()&& done; }

private:
  std::string myname;
  int num;
  bool done;
  int currentframe;
  cocos2d::Sprite* sprite1;
  cocos2d::Sprite* sprite2;

  cocos2d::Vec2 size;
  cocos2d::Action* act1;
  cocos2d::Action* act2;

  std::vector<cocos2d::Sprite* > bo;

};
#endif

#ifndef __CARTRAFIC__
#define __CARTRAFIC__

#include "cocos2d.h"
#include "base.h"
#include <list>
#include "background.h"

class CarTrafic : public cocos2d::Layer
{
public:
  CarTrafic();
  ~CarTrafic(); 
  static cocos2d::Scene* createScene();
  virtual bool init();
  void update(float);
  // void resetl();
  // a selector callback
  void menuCloseCallback(cocos2d::Ref* pSender);
  void setOb( Base* b);
    
  // implement the "static create()" method manually
  CREATE_FUNC(CarTrafic);

private:

  std::list<Base*> l;
  std::list<Base*> cross;
  Background*   sky;
  Background*   mountain;
  Background*   tree;
  Base* ob;

  cocos2d::Size visibleSize;
  cocos2d::Point origin;
  int starWidth;
};

#endif // __CARTRAFIC__

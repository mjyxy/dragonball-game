#include <iostream>
#include <cmath>
#include "enemy.h"
#include "chunk.h"
#include "gamedata.h"
#include "SimpleAudioEngine.h"


Enemy::~Enemy(){
  getParentLayer()->removeChild(sprite); 

  std::list<Base*>::iterator ptr = showlist.begin();

  while(ptr!=showlist.end()){
    delete (*ptr);
    ++ptr;
  } 

  ptr = freelist.begin();
  while(ptr!=freelist.end()){
    delete (*ptr);
    ++ptr;
  } 
  
}


Enemy::Enemy( cocos2d::Layer* p, Base* play, const std::string& name, int noFrames, int pe, float lx, float ly) : 
  Base( p ),
  myname(name),

  period(pe),
  player(play),
  showlist(),
  freelist(),  
  fd(0)
{


  std::string plistName = name +".plist";
  cocos2d::SpriteFrameCache::
    getInstance()->addSpriteFramesWithFile(plistName.c_str());

  // Set up the frames of animation:
  std::string filename = name + "%1d.png";
  cocos2d::Vector<cocos2d::SpriteFrame*>
    frames = getFrames(filename.c_str(), noFrames);

  sprite = cocos2d::Sprite::createWithSpriteFrame(frames.front());
  p->addChild(sprite, 1); 
  // sprite->release(); 
    // Lower left corner is origin; put Pter at far right, center:
  size = cocos2d::Vec2(sprite->getContentSize().width, sprite->getContentSize().height);
  sprite->setPosition(lx,ly);

  cocos2d::Animation*
  animation = cocos2d::Animation::createWithSpriteFrames(frames, 1.0f/8);
  sprite->runAction(
    cocos2d::RepeatForever::create(cocos2d::Animate::create(animation))
  );


}

Enemy::Enemy( cocos2d::Layer* p, Base* play, const std::string& name, int noFrames, int pe, float lx, float ly, 
  float vx, float vy) : 
  Base( vx, vy, p ),
  myname(name),

  period(pe),
  player(play),  
  showlist(),
  freelist(),
  fd(0)
{

  std::string plistName = name +".plist";
  cocos2d::SpriteFrameCache::
    getInstance()->addSpriteFramesWithFile(plistName.c_str());

  // Set up the frames of animation:
  std::string filename = name + "%1d.png";
  cocos2d::Vector<cocos2d::SpriteFrame*>
    frames = getFrames(filename.c_str(), noFrames);

  sprite = cocos2d::Sprite::createWithSpriteFrame(frames.front());
  p->addChild(sprite, 1); 
  // sprite->release(); 
    // Lower left corner is origin; put Pter at far right, center:
  size = cocos2d::Vec2(sprite->getContentSize().width, sprite->getContentSize().height);
  sprite->setPosition(lx,ly);

  cocos2d::Animation*
  animation = cocos2d::Animation::createWithSpriteFrames(frames, 1.0f/8);
  sprite->runAction(
    cocos2d::RepeatForever::create(cocos2d::Animate::create(animation))
  );


}


void Enemy::update(float dt) {
  cocos2d::Vec2 position = sprite->getPosition();
  cocos2d::Vec2 incr = getVelocity() * dt;
  setshift(incr.x+fd);
  fd = 0;
  sprite->setPosition(position.x + incr.x, position.y + incr.y );



  if(!isexploded()){

    if(player->isshooting()){
      if(collidedRect(player->getshoot())){
        exploded();
        player->increasescore();
      }
    }else if(!player->isexploded() && !(player->gettimer()>0) && collidedRect(player)){
        player->exploded();
    }
  }else{

    if(showlist.size()==0){
      makechunks();
      CocosDenshion::SimpleAudioEngine::
              getInstance()->playEffect("audio/exp.wav");
    }

    setshift(0);
    std::list<Base*>::iterator ptr = showlist.begin();
    while(ptr!=showlist.end()){
      (*ptr)->update(dt);
      if(this->getDistance(*ptr) > 10000){
        (*ptr)->getSprite()->setVisible(false);
        freelist.push_back((*ptr));
        ptr = showlist.erase(ptr);
      }else ++ptr;
    } 
    if(showlist.size()==0){
      exploded();
      
      sprite->setPosition( rand() % (int)(period * (getViewSize().width/2)), getPos().y);
      sprite->setVisible(true);
    }

  }
  

  cocos2d::Point location = sprite->getPosition();

  if ( location.x - getViewSize().width/2 > period * (getViewSize().width/2) + size.x/2 ) {
    sprite->setPosition(location.x - (period + 1) * (getViewSize().width/2), position.y );
  }
  if ( getViewSize().width/2 - location.x > period * (getViewSize().width/2) + size.x/2 ) {
    sprite->setPosition((period + 1) * (getViewSize().width/2) + location.x, position.y );
  }  
  if ( location.y > getViewSize().height + size.x/2 ) {
    sprite->setPosition(position.x, -size.x/2 );
  }  
  if ( location.y < -size.x/2 ) {
    sprite->setPosition(position.x, getViewSize().height + size.x/2 );
  }  


}

float Enemy::getDistance( const Base* b ) const {
  return sprite->getPosition().distanceSquared(b->getPos());
}

cocos2d::Vector<cocos2d::SpriteFrame*> 
Enemy::getFrames(const char *format, int count) {
  cocos2d::SpriteFrameCache* 
  spritecache = cocos2d::SpriteFrameCache::getInstance();
  cocos2d::Vector<cocos2d::SpriteFrame*> animFrames;
  char str[100];
  for(int i = 1; i <= count; i++) {
    sprintf(str, format, i);
    animFrames.pushBack(spritecache->getSpriteFrameByName(str));
  }
  return animFrames;
}


void Enemy::makechunks(){

  sprite->setVisible(false);

  // When new a chunk, constructor will call addChild.
  // Otherwise, we will addChild at here.
  int chunksizex = Gamedata::getInstance().getXmlInt("enemy/chunksizex");
  int chunksizey = Gamedata::getInstance().getXmlInt("enemy/chunksizey");

  if(freelist.size()>0){    
    std::list<Base*>::iterator ptr = freelist.begin();
    while(ptr!=freelist.end()){
      (*ptr)->getSprite()->setVisible(true);
      showlist.push_back(*ptr);
      ptr=freelist.erase(ptr);
    }
  }
  else{
    int tx = getSize().x/chunksizex;
    int ty = getSize().y/chunksizey;
    for(int i=0;i<chunksizex;i++){
      for(int j=0;j<chunksizey;j++){
        showlist.push_back(new Chunk( getParentLayer(), myname+"1.png", getPos().x, getPos().y, 0, 0, i*tx, j*ty, tx, ty));
      }
    }
  }
  resetshowlist();
}

void Enemy::resetshowlist(){
  float px = getPos().x;
  float py = getPos().y;
  int chunksizex = Gamedata::getInstance().getXmlInt("enemy/chunksizex");
  int chunksizey = Gamedata::getInstance().getXmlInt("enemy/chunksizey");
  int tx = getSize().x/chunksizex;
  int ty = getSize().y/chunksizey;
  std::list<Base*>::iterator ptr = showlist.begin();
  int i=0,j=0;
  int basespeed = Gamedata::getInstance().getXmlInt("enemy/basespeed");;
  int flospeed = Gamedata::getInstance().getXmlInt("enemy/flospeed");;
  float theta;
  int up;

  while (ptr != showlist.end()){
    theta = rand()%360* (M_PI) / 360;
    float temp = basespeed + rand()%flospeed - flospeed/2;
    up = rand()%2?1:-1;
    (*ptr)->setVelocity(cocos2d::Vec2(up*temp * cos(theta), up*temp*sin(theta))); 
    (*ptr)->getSprite()->setPosition(cocos2d::Vec2(px-tx*(chunksizex-2*i-1)/(2*chunksizex), 
      py+ty*(chunksizey-2*j-1)/(2*chunksizey)));
    i=(i+1)%chunksizex;
    if(i==0) j++;
    ++ptr;
  }

}



#ifndef GOKU__H
#define GOKU__H
#include "cocos2d.h"
#include "kame.h"
#include <cstring>

class Goku : public Base {
public:
  Goku(cocos2d::Layer* p, const std::string& name1, const std::string& name2, int noFrames1, int noFrames2, float lx, float ly);
  virtual ~Goku();
  virtual void update(float);
  cocos2d::Sprite* getSprite() const;
  cocos2d::Vector<cocos2d::SpriteFrame*> getFrames(const char *format, int count);

  float getDistance( const Base* b ) const ;
  cocos2d::Vec2 getPos() const { return standsprite->getPosition(); }

  void makechunks();
  void resetshowlist();

  cocos2d::Vec2 getSize();
  void setfd(float f) { fd = f; }
  bool isshooting() { if(k!=NULL) return true; return false; }
  Base* getshoot() { if(k!=NULL) return k; return NULL; }
private:
  int num1;  
  int num2;  
  cocos2d::Sprite* standsprite;
  cocos2d::Sprite* movesprite;
  cocos2d::Vec2 size1;
  cocos2d::Vec2 size2;
  bool moveLeft, moveRight, moveUp, moveDown,facetoright;

  std::list<Base*> showlist;
  std::list<Base*> freelist;
  cocos2d::Label* label;
  cocos2d::Label* label2;
  float fd;
  Kame* k;
};
#endif

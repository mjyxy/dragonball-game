#ifndef BASE__H
#define BASE__H
#include "cocos2d.h"

/*
 *  This is the base class with vel, viewsize, originpos
 *  and parent layer pointer ( we can get position from
 *  cocos2d::sprite). 
 *
 *  Maybe I should save name string in sprite class.
 */

class Base {
public:
    Base( cocos2d::Layer* );
  	Base(float, float, cocos2d::Layer* );
  	virtual ~Base() {}

  	cocos2d::Vec2 getVelocity() const { return velocity; }
  	void setVelocity( const cocos2d::Vec2& v ) { velocity.set(v); }
  	float VelX() { return velocity.x; }
  	void VelX( float x ) { velocity.x = x; }
  	float VelY() { return velocity.y; }
  	void VelY( float y ) { velocity.y = y; }
  	// cocos2d::Vector<cocos2d::SpriteFrame*> getFrames(const char *format, int count);
  	cocos2d::Size getViewSize() const { return viewSize; }

    virtual cocos2d::Vec2 getSize() = 0;
  	virtual void update(float) = 0;  	
  	virtual cocos2d::Sprite* getSprite() const = 0;
  	virtual float getDistance( const Base* b ) const = 0;
  	virtual cocos2d::Vec2 getPos() const = 0;
  	// virtual bool isexplode() = 0;
    cocos2d::Layer* getParentLayer() { return parent; }
    bool collidedRect(const Base* b) const;
    virtual void getObserve(std::list<Base*>&) {}
    bool isexploded() { return explode; }
    void exploded() { explode = !explode; }
    virtual void reset() {}
    virtual bool isidle() { return false; }
    float getshift() { return shift; }
    void setshift(float f) { shift = f; }
    void resettimer(int a) { timer = a; }
    void detimer(int b) { timer -= b; }
    int gettimer() { return timer; }
    virtual bool isshooting() { return false; }
    virtual Base* getshoot() { return NULL; }
    virtual void flash() {}
    void increasescore() { score++; }
    void resetscore() { score=0; }
    int getscore() { return score; }

private:
    int timer;
    int score;
  	cocos2d::Vec2 velocity;
  	cocos2d::Size viewSize;
  	cocos2d::Point origin;
    bool explode;
    float shift;
    cocos2d::Layer* parent; 
};
#endif

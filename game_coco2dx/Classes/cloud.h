#ifndef CLOUD__H
#define CLOUD__H
#include "cocos2d.h"
#include "base.h"
#include <cstring>


class Cloud : public Base {
public:
  Cloud(cocos2d::Layer* p, const std::string& name, int pe, float lx, float ly);
  Cloud(cocos2d::Layer* p, const std::string& name, int pe, float lx, float ly, 
    float vx, float vy);
  Cloud(cocos2d::Layer* p, const std::string& name, int pe, float lx, float ly, 
    float vx, float vy, float s);

  virtual ~Cloud() { /*delete sprite;*/ getParentLayer()->removeChild(sprite); }
  virtual void update(float);
  cocos2d::Sprite* getSprite() const { return sprite; }
    
  float getDistance( const Base* b ) const ;
  cocos2d::Vec2 getPos() const { return sprite->getPosition(); }
  // bool isexplode() { return false; }
  cocos2d::Vec2 getSize() { return size; }
  void setfd(float f) { fd = f; }
  // virtual bool collidedRect(const Base* b);
private:
  cocos2d::Sprite* sprite;
  cocos2d::Vec2 size;
  int period;
  float fd;
};
#endif

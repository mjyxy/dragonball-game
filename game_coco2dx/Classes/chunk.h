#ifndef CHUNK__H
#define CHUNK__H
#include "cocos2d.h"
#include "base.h"
#include <cstring>

/*
 *  Chunk is general sprite.
 *
 */

class Chunk : public Base {
public:
  	Chunk( cocos2d::Layer* p, const std::string& name, float lx, float ly, 
  		float vx, float vy, int ox, int oy, int w, int h);
  	virtual ~Chunk() { /*delete sprite;*/ /*getParentLayer()->removeChild(sprite);*/ }
  	void virtual update(float);
  	cocos2d::Sprite* getSprite() const { return sprite; }
    
  	float getDistance( const Base* b ) const ;
  	cocos2d::Vec2 getPos() const { return sprite->getPosition(); }
	// bool isexplode() { return false; }
	cocos2d::Vec2 getSize() { return size; }

private:
  	cocos2d::Sprite* sprite;
  	cocos2d::Vec2 size;
};
#endif

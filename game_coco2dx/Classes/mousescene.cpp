#include "mousescene.h"
#include <iostream>

// cocos2d::Scene* MouseScene::createScene()
// {
//     cocos2d::Scene* scene = cocos2d::Scene::create();
//     cocos2d::Layer* layer = MouseScene::create();
//     scene->addChild(layer);

//     return scene;
// }

MouseScene::MouseScene(Base* b) :
  ob(b)
{ }

MouseScene* MouseScene::create(Base* b) {
    MouseScene* backLayer = new MouseScene(b);
    if (backLayer && backLayer->init()) {
        backLayer->autorelease();
        return backLayer;
    }
    CC_SAFE_DELETE(backLayer);
    return NULL;
}

void MouseScene::analymouse(int num, float x, float y){
  if(num == 1) ob->flash();
}

bool MouseScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }   

   cocos2d::EventListenerMouse* listener = cocos2d::EventListenerMouse::create();
   listener->onMouseDown = [&](cocos2d::Event* event){

      // try {
         cocos2d::EventMouse* mouseEvent = dynamic_cast<cocos2d::EventMouse*>(event);
         mouseEvent->getMouseButton();
         std::stringstream message;
         message << "Mouse event: Button: " << mouseEvent->getMouseButton() << "pressed at point (" <<
            mouseEvent->getLocation().x << "," << mouseEvent->getLocation().y << ")";
         cocos2d::MessageBox(message.str().c_str(), "Mouse Event Details");
         // if(mouseEvent->getMouseButton() == 1) ob->flash();
         analymouse(mouseEvent->getMouseButton(), mouseEvent->getLocation().x, mouseEvent->getLocation().y );
      // }
      // catch (std::bad_cast& e){
         // Not sure what kind of event you passed us cocos, but it was the wrong one
         // return;
      // }
   };

   listener->onMouseMove = [](cocos2d::Event* event){
      // Cast Event to EventMouse for position details like above
      cocos2d::log("Mouse moved event");
   };

   listener->onMouseScroll = [](cocos2d::Event* event){
      cocos2d::log("Mouse wheel scrolled");
   };

   listener->onMouseUp = [](cocos2d::Event* event){
      cocos2d::log("Mouse button released");
   };

   _eventDispatcher->addEventListenerWithFixedPriority(listener, 1);

    return true;
}

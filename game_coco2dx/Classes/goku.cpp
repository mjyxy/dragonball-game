#include <iostream>
#include <cmath>
#include "goku.h"
#include "chunk.h"
#include "gamedata.h"
#include <sstream>
#include "SimpleAudioEngine.h"

Goku::~Goku(){

  if(k!=NULL) delete k; 
  getParentLayer()->removeChild(standsprite); 
  getParentLayer()->removeChild(movesprite); 

  std::list<Base*>::iterator ptr = showlist.begin();

  while(ptr!=showlist.end()){
    delete (*ptr);
    ++ptr;
  } 

  ptr = freelist.begin();
  while(ptr!=freelist.end()){
    delete (*ptr);
    ++ptr;
  } 
  
}

cocos2d::Sprite* Goku::getSprite() const { 
  return standsprite; 
}


cocos2d::Vec2 Goku::getSize() {
  if(k!=NULL) return k->getSize(); 
  if(VelX()!=0) return size2;
  return size1; 
}

Goku::Goku( cocos2d::Layer* p, const std::string& name1, const std::string& name2, int noFrames1, int noFrames2, float lx, float ly) : 
  Base( p ),
  num1(noFrames1),
  num2(noFrames2),
  moveLeft(false), moveRight(false), moveUp(false), moveDown(false),facetoright(true),
  showlist(),
  freelist(),  
  label(cocos2d::Label::createWithTTF("Eliminate 10 Enemies", "fonts/Marker Felt.ttf", 24)),
  label2(cocos2d::Label::createWithTTF("You Win!", "fonts/Marker Felt.ttf", 100)),
  fd(0),
  k(NULL)
{


  label->setPosition(
    cocos2d::Vec2(cocos2d::Director::getInstance()->getVisibleOrigin().x + cocos2d::Director::getInstance()->getVisibleSize().width - label->getContentSize().width,
         cocos2d::Director::getInstance()->getVisibleOrigin().y + cocos2d::Director::getInstance()->getVisibleSize().height - label->getContentSize().height)
  );
  label->setColor(cocos2d::Color3B(249,87,0));
  // add the label as a child to this layer
  p->addChild(label, 1);

  label2->setPosition(
    cocos2d::Vec2(cocos2d::Director::getInstance()->getVisibleOrigin().x + cocos2d::Director::getInstance()->getVisibleSize().width/2,
         cocos2d::Director::getInstance()->getVisibleOrigin().y + cocos2d::Director::getInstance()->getVisibleSize().height/2 )
  );
  label2->setColor(cocos2d::Color3B(100,100,255));
  // add the label as a child to this layer
  p->addChild(label2, 1);
  label2->setVisible(false);


  std::string plistName = name1 +".plist";
  cocos2d::SpriteFrameCache::
    getInstance()->addSpriteFramesWithFile(plistName.c_str());

  // Set up the frames of animation:
  std::string filename = name1 + "%1d.png";
  cocos2d::Vector<cocos2d::SpriteFrame*>
    frames = getFrames(filename.c_str(), noFrames1);

  standsprite = cocos2d::Sprite::createWithSpriteFrame(frames.front());
  p->addChild(standsprite, 1); 
  // sprite->release(); 
    // Lower left corner is origin; put Pter at far right, center:
  size1 = cocos2d::Vec2(standsprite->getContentSize().width, standsprite->getContentSize().height);
  standsprite->setPosition(lx,ly);

  cocos2d::Animation*
  animation = cocos2d::Animation::createWithSpriteFrames(frames, 1.0f/8);
  standsprite->runAction(
    cocos2d::RepeatForever::create(cocos2d::Animate::create(animation))
  );

  plistName = name2 +".plist";
  cocos2d::SpriteFrameCache::
    getInstance()->addSpriteFramesWithFile(plistName.c_str());

  // Set up the frames of animation:
  filename = name2 + "%1d.png";

  frames = getFrames(filename.c_str(), noFrames2);

  movesprite = cocos2d::Sprite::createWithSpriteFrame(frames.front());
  p->addChild(movesprite, 1); 
  // sprite->release(); 
    // Lower left corner is origin; put Pter at far right, center:
  size2 = cocos2d::Vec2(movesprite->getContentSize().width, movesprite->getContentSize().height);
  movesprite->setPosition(lx,ly);

  cocos2d::Animation*
  animation2 = cocos2d::Animation::createWithSpriteFrames(frames, 1.0f/8);
  movesprite->runAction(
    cocos2d::RepeatForever::create(cocos2d::Animate::create(animation2))
  );

  movesprite->setVisible(false);  // not display

  cocos2d::EventListenerKeyboard* l = 
   cocos2d::EventListenerKeyboard::create();
  l->onKeyPressed =
    [this](cocos2d::EventKeyboard::KeyCode keyCode,cocos2d::Event* event) {
    switch(keyCode) {
      case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
      case cocos2d::EventKeyboard::KeyCode::KEY_A:
        moveLeft = true;
        break;
      case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
      case cocos2d::EventKeyboard::KeyCode::KEY_D:
        moveRight = true;
        break;
      case cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW:
      case cocos2d::EventKeyboard::KeyCode::KEY_S:
        moveDown = true;
        break;
      case cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW:
      case cocos2d::EventKeyboard::KeyCode::KEY_W:
        moveUp = true;
        break;
      default:
        break;
    }
  };

  l->onKeyReleased =
    [this](cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event){
    switch(keyCode) {
      case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
      case cocos2d::EventKeyboard::KeyCode::KEY_A:
        moveLeft = false;
        break;
      case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
      case cocos2d::EventKeyboard::KeyCode::KEY_D:
        moveRight = false;
        break;
      case cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW:
      case cocos2d::EventKeyboard::KeyCode::KEY_S:
        moveDown = false;
        break;
      case cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW:
      case cocos2d::EventKeyboard::KeyCode::KEY_W:
        moveUp = false;
        break;
      default:
        break;
    }
  };

  cocos2d::Director::getInstance()->getEventDispatcher()->
    addEventListenerWithFixedPriority(l, 1);

}

void Goku::update(float dt) {
  std::stringstream ss;
  if(10-getscore() <=0){
    resettimer(10000);
      label2->setVisible(true);
      ss << "Eliminate 0 Enemies";
      label->setString(ss.str());
  }else{
    ss << "Eliminate " <<10-getscore()<<" Enemies";
    label->setString(ss.str());

    VelX(0);

    // if(moveUp && !isexploded()) exploded();


    if(isexploded()){
      resetscore();
      if(showlist.size()==0){
        makechunks();
        CocosDenshion::SimpleAudioEngine::
              getInstance()->playEffect("audio/exp.wav");
      }

      setshift(0);
      std::list<Base*>::iterator ptr = showlist.begin();
      while(ptr!=showlist.end()){
        (*ptr)->update(dt);
        if(this->getDistance(*ptr) > 10000){
          (*ptr)->getSprite()->setVisible(false);
          freelist.push_back((*ptr));
          ptr = showlist.erase(ptr);
        }else ++ptr;
      } 
      if(showlist.size()==0){
        exploded();
        resettimer(1000);
        standsprite->setVisible(true);
      }


    }else{
      if(gettimer()>0) detimer(dt*1000);
      if(moveDown || k!=NULL){
          standsprite->setVisible(false);
          movesprite->setVisible(false);
        if(k==NULL){
          k=new Kame( getParentLayer(), "kame", Gamedata::getInstance().getXmlInt("kame/framenum"), getPos().x, getPos().y, facetoright);
          CocosDenshion::SimpleAudioEngine::
              getInstance()->playEffect("audio/Goku_Kamehameha.mp3");
        }
        k->update(dt);
        if(k->isD()){
          delete k;
          k=NULL;
          standsprite->setVisible(true);
          movesprite->setVisible(false);
        }
      }else{

        if( moveLeft && !moveRight) {
          VelX( -200 );
          facetoright = false;
          standsprite->setVisible(false);
          movesprite->setVisible(true);
        }  
        else if( !moveLeft && moveRight) {
          VelX( 200 );
          facetoright = true;
          standsprite->setVisible(false);
          movesprite->setVisible(true);
        }
        else{
          standsprite->setVisible(true);
          movesprite->setVisible(false);  
        }


        standsprite->setScaleX(facetoright?1.0:-1.0);
        movesprite->setScaleX(facetoright?1.0:-1.0);
      }

      cocos2d::Vec2 position = standsprite->getPosition();
      cocos2d::Vec2 incr = getVelocity() * dt;
      setshift(incr.x+fd);
      fd = 0;
      standsprite->setPosition(position.x + incr.x, position.y + incr.y );
      movesprite->setPosition(position.x + incr.x, position.y + incr.y );  
    }
  }


}

float Goku::getDistance( const Base* b ) const {
  return standsprite->getPosition().distanceSquared(b->getPos());
}

cocos2d::Vector<cocos2d::SpriteFrame*> 
Goku::getFrames(const char *format, int count) {
  cocos2d::SpriteFrameCache* 
  spritecache = cocos2d::SpriteFrameCache::getInstance();
  cocos2d::Vector<cocos2d::SpriteFrame*> animFrames;
  char str[100];
  for(int i = 1; i <= count; i++) {
    sprintf(str, format, i);
    animFrames.pushBack(spritecache->getSpriteFrameByName(str));
  }
  return animFrames;
}


void Goku::makechunks(){


    standsprite->setVisible(false);
    movesprite->setVisible(false);

  // When new a chunk, constructor will call addChild.
  // Otherwise, we will addChild at here.
  int chunksizex = Gamedata::getInstance().getXmlInt("goku/chunksizex");
  int chunksizey = Gamedata::getInstance().getXmlInt("goku/chunksizey");

  if(freelist.size()>0){    
    std::list<Base*>::iterator ptr = freelist.begin();
    while(ptr!=freelist.end()){
      (*ptr)->getSprite()->setVisible(true);
      showlist.push_back(*ptr);
      ptr=freelist.erase(ptr);
    }
  }
  else{
    int tx = getSize().x/chunksizex;
    int ty = getSize().y/chunksizey;
    for(int i=0;i<chunksizex;i++){
      for(int j=0;j<chunksizey;j++){
        showlist.push_back(new Chunk( getParentLayer(), "goku.png", getPos().x, getPos().y, 0, 0, i*tx, j*ty, tx, ty));
      }
    }
  }
  resetshowlist();
}

void Goku::resetshowlist(){
  float px = getPos().x;
  float py = getPos().y;
  int chunksizex = Gamedata::getInstance().getXmlInt("goku/chunksizex");
  int chunksizey = Gamedata::getInstance().getXmlInt("goku/chunksizey");
  int tx = getSize().x/chunksizex;
  int ty = getSize().y/chunksizey;
  std::list<Base*>::iterator ptr = showlist.begin();
  int i=0,j=0;
  int basespeed = Gamedata::getInstance().getXmlInt("goku/basespeed");;
  int flospeed = Gamedata::getInstance().getXmlInt("goku/flospeed");;
  float theta;
  int up;

  while (ptr != showlist.end()){
    theta = rand()%360* (M_PI) / 360;
    float temp = basespeed + rand()%flospeed - flospeed/2;
    up = rand()%2?1:-1;
    (*ptr)->setVelocity(cocos2d::Vec2(up*temp * cos(theta), up*temp*sin(theta))); 
    (*ptr)->getSprite()->setPosition(cocos2d::Vec2(px-tx*(chunksizex-2*i-1)/(2*chunksizex), 
      py+ty*(chunksizey-2*j-1)/(2*chunksizey)));
    i=(i+1)%chunksizex;
    if(i==0) j++;
    ++ptr;
  }

}


#include <iostream>
#include <cmath>
#include "chunk.h"

Chunk::Chunk( cocos2d::Layer* p, const std::string& name, float lx, float ly, 
	float vx, float vy, int ox, int oy, int w, int h):
  Base( vx, vy, p ),
  sprite( cocos2d::Sprite::create(name, cocos2d::Rect(ox, oy, w, h))),
  size( sprite->getContentSize().width, sprite->getContentSize().height )
{
  getParentLayer()->addChild(sprite);
  sprite->setPosition( cocos2d::Point(lx, ly) ); 

}

void Chunk::update(float dt) {
  cocos2d::Vec2 position = sprite->getPosition();
  cocos2d::Vec2 incr = getVelocity() * dt;
  sprite->setPosition(position.x + incr.x, position.y + incr.y );
}

float Chunk::getDistance( const Base* b ) const {
  return sprite->getPosition().distanceSquared(b->getPos());
}
#ifndef SMART__H
#define SMART__H
#include "cocos2d.h"
#include "base.h"
#include <cstring>
#include "enemy.h"

class Smart : public Enemy {
public:
  Smart(cocos2d::Layer* p, Base* play, const std::string& name, int noFrames, 
    const std::string& name2, int noFrames2, int pe, float lx, float ly, float vy, float ythreshold );


  virtual ~Smart() { getParentLayer()->removeChild(sprite2); }
  virtual void update(float);

private:

  cocos2d::Sprite* sprite2;
  float speedy;
  float threshold;
  float oriy;

};
#endif

#include "carTrafic.h"
#include "goku.h"
#include "enemy.h"
#include "smart.h"
#include "cloud.h"
#include "gamedata.h"
#include <iostream>
#include "mousescene.h"
#include "SimpleAudioEngine.h"

CarTrafic::CarTrafic() : 
  l(),
  cross()
{ 

  sky = Background::create("sky.png", 0.25);
  addChild(sky,  -10); 
  mountain = Background::create("mountain.png", 0.5);
  addChild(mountain,  -5); 
  tree = Background::create("tree.png", 1);
  addChild(tree,  -1); 

  l.push_back(new Goku( this, "s", "r", Gamedata::getInstance().getXmlInt("goku/framenum1"),
    Gamedata::getInstance().getXmlInt("goku/framenum2"),
    Gamedata::getInstance().getXmlInt("goku/px"),
    Gamedata::getInstance().getXmlInt("goku/py")));


  int basespeed = Gamedata::getInstance().getXmlInt("cloud/basespeed");
  int flospeed = Gamedata::getInstance().getXmlInt("cloud/flospeed");
  int maxy = Gamedata::getInstance().getXmlInt("cloud/maxy");
  float s = basespeed;

  int skynum = Gamedata::getInstance().getXmlInt("cloud/sky");
  int mountainnum = Gamedata::getInstance().getXmlInt("cloud/mountain");
  int treenum = Gamedata::getInstance().getXmlInt("cloud/tree");

  for(int i=0;i<skynum;i++){
    s += flospeed/skynum;
    l.push_back(new Cloud(sky, "cloud.png", skynum, rand() % (1024*skynum) , 768-rand()%maxy, s/4, 0, 0.1+i*0.03));
  }

  s = basespeed;

  for(int i=0;i<mountainnum;i++){
    s += flospeed/mountainnum;
    l.push_back(new Cloud(mountain, "cloud.png", mountainnum, rand() % (1024*mountainnum) , 768-rand()%maxy, s/2, 0, 0.5+i*0.02));
  }

  s = basespeed;

  for(int i=0;i<treenum;i++){
    s += flospeed/treenum;
    l.push_back(new Cloud(tree, "cloud.png", treenum, rand() % (1024*treenum) , 768-rand()%maxy, s, 0, 0.8+i*0.02));
  }

  for(int i=0;i<5;i++){
    l.push_back(new Enemy(tree, *l.begin(), "drum", Gamedata::getInstance().getXmlInt("drum/framenum"), 10, rand()%5120,  
      Gamedata::getInstance().getXmlInt("drum/py")));

    l.push_back(new Smart(tree, *l.begin(), "tam", Gamedata::getInstance().getXmlInt("tam/framenum"), "dis" , 
      Gamedata::getInstance().getXmlInt("dis/framenum"), 10, rand()%5120,  Gamedata::getInstance().getXmlInt("tam/py"), 200, 400));
  }


}

CarTrafic::~CarTrafic() {  
  std::list<Base*>::iterator ptr = l.begin(); // delete
  while(ptr!=l.end()){
    delete (*ptr);
    ++ptr;
  } 
}

void CarTrafic::update(float dt) {
  ob->update(dt);

  std::list<Base*>::iterator ptr = l.begin(); // Polymorphic updating
  while(ptr!=l.end()){
    if((*ptr) != ob) (*ptr)->update(dt);
    ++ptr;
  }
  float index = ob->getshift();
  ptr = l.begin();
  while(ptr!=l.end()){
    (*ptr)->getSprite()->setPosition((*ptr)->getPos() - cocos2d::Vec2(index, 0));
    ++ptr;
  }
  sky->update(index);
  mountain->update(index);
  tree->update(index);

}


cocos2d::Scene* CarTrafic::createScene() {
  // 'scene' is an autorelease object
  cocos2d::Scene* scene = cocos2d::Scene::create();
    
  // 'layer' is an autorelease object
  cocos2d::Layer* layer = CarTrafic::create();

  // add layer as a child to scene
  scene->addChild(layer);


  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool CarTrafic::init() {
  //////////////////////////////
  // 1. super init first
  if ( !Layer::init() ) {
    return false;
  }
    
  // cocos2d::Size 
  visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
  // cocos2d::Vec2 
  origin = cocos2d::Director::getInstance()->getVisibleOrigin();

  /////////////////////////////
  // 2. add a menu item with "X" image, which is clicked to quit the program
  //    you may modify it.

  // add a "close" icon to exit the progress. it's an autorelease object
  cocos2d::MenuItemImage* closeItem = cocos2d::MenuItemImage::create(
                      "CloseNormal.png",
                      "CloseSelected.png",
                      CC_CALLBACK_1(CarTrafic::menuCloseCallback, this));
    
	closeItem->setPosition(
    cocos2d::Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
         origin.y + closeItem->getContentSize().height/2)
  );

  // create menu, it's an autorelease object
  cocos2d::Menu* menu = cocos2d::Menu::create(closeItem, NULL);
  menu->setPosition(cocos2d::Vec2::ZERO);
  this->addChild(menu, 10);

  /////////////////////////////
  // 3. add your codes below...

  cocos2d::Label* 
  label = 
    cocos2d::Label::createWithTTF("Dragon ball----Jingyao Ma", "fonts/Marker Felt.ttf", 24);
  label->setColor(cocos2d::Color3B(249,87,0));
  // position the background on the center of the screen
  label->setPosition(
    cocos2d::Vec2(origin.x + visibleSize.width/2,
         origin.y + visibleSize.height - label->getContentSize().height)
  );

  // add the label as a child to this layer
  addChild(label, 1);

  
  setOb(*l.begin());

  CocosDenshion::SimpleAudioEngine::
    getInstance()->playBackgroundMusic("audio/8bitPacific_Rim.mp3");
  schedule( schedule_selector(CarTrafic::update));

  return true;
}


void CarTrafic::menuCloseCallback(Ref* pSender) {

  cocos2d::Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
  exit(0);
#endif
}

void CarTrafic::setOb( Base* b){

  ob = b;
  // set the observed sprite at central, then shift all other stuff
  float del = visibleSize.width/2 + origin.x - b->getPos().x;

  b->getSprite()->setPosition(visibleSize.width/2 + origin.x, ob->getPos().y );

  auto ptr = l.begin();
  while(ptr!=l.end()){
    if((*ptr) != ob) (*ptr)->getSprite()->setPosition((*ptr)->getPos() - cocos2d::Vec2(del+visibleSize.width/2, 0)); // because background
    ++ptr;
  }
  sky->update(del);
  mountain->update(del);
  tree->update(del);

}
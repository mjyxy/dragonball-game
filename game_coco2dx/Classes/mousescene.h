#ifndef __MOUSESCENE__
#define __MOUSESCENE__

#include "cocos2d.h"
#include "base.h"

class MouseScene : public cocos2d::Layer
{

    public:
    	MouseScene(Base* b);
        // static cocos2d::Scene* createScene();
        virtual bool init();
    	static MouseScene* create(Base* b);

    	void analymouse(int, float, float);

        
        // CREATE_FUNC(MouseScene);
	private:
		Base* ob;

};
#endif

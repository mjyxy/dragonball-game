#include <iostream>
#include <cmath>
#include "base.h"

Base::Base( cocos2d::Layer* p ) :
  timer(0),
  score(0),
  velocity( 0, 0 ),
  viewSize( cocos2d::Director::getInstance()->getVisibleSize() ),
  origin( cocos2d::Director::getInstance()->getVisibleOrigin() ),
  explode(false),
  shift(0),
  parent(p)
{
}

Base::Base(float x, float y, cocos2d::Layer* p ) : 
  timer(0),
  score(0),
  velocity( x, y ),
  viewSize( cocos2d::Director::getInstance()->getVisibleSize() ),
  origin( cocos2d::Director::getInstance()->getVisibleOrigin() ),
  explode(false),
  shift(0),
  parent(p)
{
}

bool Base::collidedRect(const Base* otherSprite) const {
  int myWidth = getSprite()->getContentSize().width; 
  int myHeight = getSprite()->getContentSize().height; 
  int oWidth = otherSprite->getSprite()->getContentSize().width; 
  int oHeight = otherSprite->getSprite()->getContentSize().height; 
  
  cocos2d::Point myPos = getSprite()->getPosition();
  cocos2d::Point oPos  = otherSprite->getSprite()->getPosition();

  if ( myPos.x+myWidth/2 < oPos.x-oWidth/2 ) return false;
  if ( myPos.x-myWidth/2 > oPos.x+oWidth/2 ) return false;
  if ( myPos.y-myHeight/2 > oPos.y+oHeight/2 ) return false;
  if ( myPos.y+myHeight/2 < oPos.y-oHeight/2 ) return false;

  return true;
}
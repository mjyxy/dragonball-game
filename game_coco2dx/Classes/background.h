#ifndef BACKGROUND__H
#define BACKGROUND__H
#include <string>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include "cocos2d.h"

class Background : public cocos2d::Layer {
public:
  Background(float s);
  void update(float dt);
  virtual bool init(const std::string&);
  cocos2d::Sprite* stretchFit(cocos2d::Sprite* const sprite);
  // We're going to override the magical creation of the create function:
  //CREATE_FUNC(Background);
  static Background* create(const std::string&, float s);

  void setBackgroundFile(const std::string& bf) { backFile = bf; }

private:
  cocos2d::Vec2 origin;
  cocos2d::Size viewSize;
  std::string backFile;

  cocos2d::Sprite* bg1;
  cocos2d::Sprite* bg2;

  float scrollSpeed;
};
#endif

#include <string>
#include <SDL.h>
#include <vector>

class MultiSprite {
public:
  MultiSprite(SDL_Surface* src, const std::string& filename, int frames, int cnt, Uint16 width, Uint16 height, float pX, float pY, float vX, float vY, bool setColorKey=false, int sci =255, int m=0);

  MultiSprite(const MultiSprite& s);
  ~MultiSprite();
  MultiSprite& operator=(const MultiSprite&);

  void draw() const;
  void draw(Sint16, Sint16) const;
  void update(Uint32 ticks);
  float getDistance(const MultiSprite& ) const;
  bool isDone() const { return finish; }
  float X() const { return posX; }
  float Y() const { return posY; }
private:
  SDL_Surface* screen;
  std::vector<SDL_Surface*> surfs;

  float posX;
  float posY;
  float velX;
  float velY;
  int cur;
  int count;
  int remind;
  int framecnt;

  int model;
  bool finish;

};
#include <cmath>
#include "sprite.h"

Sprite::Sprite(SDL_Surface* src, const std::string& filename, float pX, float pY, float vX, float vY, bool setColorKey) :
	screen(src),
	surf(NULL),
	posX(pX),
	posY(pY),
	velX(vX),
	velY(vY)
	{
  	SDL_Surface *temp = SDL_LoadBMP(filename.c_str());
	if (temp == NULL) {
	  throw std::string("Unable to load bitmap.")+SDL_GetError();
	}
	if ( setColorKey ) {
	  Uint32 colorkey = SDL_MapRGB(temp->format, 255, 0, 255);
	  SDL_SetColorKey(temp, SDL_SRCCOLORKEY|SDL_RLEACCEL, colorkey);
	}
	surf = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);
}

Sprite::Sprite(const Sprite& s) :
	screen(s.screen),
	surf(s.surf),
	posX(s.posX),
	posY(s.posY),
	velX(s.velX),
	velY(s.velY)
	{
}

Sprite& Sprite::operator=(const Sprite& rhs){
	if(this == &rhs) return *this;
	screen=rhs.screen;
	SDL_FreeSurface(surf);
	surf=rhs.surf;
	posX=rhs.posX;
	posY=rhs.posY;
	velX=rhs.velX;
	velY=rhs.velY;
	return *this;
}

void Sprite::draw() const {
  	Uint16 w = surf->w;
  	Uint16 h = surf->h;
  	Sint16 xCoord = static_cast<Sint16>(posX);
  	Sint16 yCoord = static_cast<Sint16>(posY);
  	SDL_Rect src = { 0, 0, w, h };
  	SDL_Rect dest = {xCoord, yCoord, 0, 0 };
  	SDL_BlitSurface(surf, &src, screen, &dest);
}

void Sprite::draw(Sint16 x, Sint16 y) const {
  	Uint16 w = surf->w;
  	Uint16 h = surf->h;
  	SDL_Rect src = { 0, 0, w, h };
  	SDL_Rect dest = {x, y, 0, 0 };
  	SDL_BlitSurface(surf, &src, screen, &dest);
}

void Sprite::update(Uint32 ticks){

  	float incrX = velX * static_cast<float>(ticks) * 0.001; 
  	float incrY = velY * static_cast<float>(ticks) * 0.001;
  	posX+=incrX;
  	posY+=incrY;

}

float Sprite::getDistance(const Sprite& s) const{
	return hypot(posX-s.posX, posY-s.posY);
}
#include <cmath>
#include "multisprite.h"
#include "extractSurface.h"

MultiSprite::~MultiSprite(){
  for(int i=0;i<framecnt;i++){
    SDL_FreeSurface(surfs[i]);
  }
}

MultiSprite::MultiSprite(SDL_Surface* src, const std::string& filename, int frames, int cnt, Uint16 width, Uint16 height, float pX, float pY, float vX, float vY, bool setColorKey, int sci, int m) :
  screen(src),
  surfs(),
  posX(pX),
  posY(pY),
  velX(vX),
  velY(vY),
  cur(0),
  count(cnt),
  remind(count),
  framecnt(frames),
  model(m),
  finish(false)
  {
    surfs.reserve(framecnt);
    SDL_Surface *temp = SDL_LoadBMP(filename.c_str());
    if (temp == NULL) {
      throw std::string("Unable to load bitmap.")+SDL_GetError();
    }
    SDL_Surface* surf;
    for (int i = 0; i < framecnt; ++i) {
      int frameX = i * width;
      surf = ExtractSurface::getInstance().
                 get(temp, width, height, frameX, 0); 
      if ( setColorKey ) {
        Uint32 colorkey = SDL_MapRGB(surf->format, sci, 0, sci);
        SDL_SetColorKey(surf, SDL_SRCCOLORKEY|SDL_RLEACCEL, colorkey);
      }
      surfs.push_back( surf );
    }
    SDL_FreeSurface(temp);
}

MultiSprite::MultiSprite(const MultiSprite& s) :
  screen(s.screen),
  surfs(),
  posX(s.posX),
  posY(s.posY),
  velX(s.velX),
  velY(s.velY),
  cur(0),
  count(s.count),
  remind(count),
  framecnt(s.framecnt),
  model(s.model),
  finish(false)
  {
    surfs.reserve(framecnt);
    for(int i=0;i<framecnt;i++){
      surfs.push_back(s.surfs[i]);
    }
}

MultiSprite& MultiSprite::operator=(const MultiSprite& rhs){
  if(this == &rhs) return *this;
  screen=rhs.screen;

  for(int i=0;i<framecnt;i++){
    SDL_FreeSurface(surfs[i]);
  }
  for(int i=0;i<rhs.framecnt;i++){
    surfs.push_back(rhs.surfs[i]);
  }
  posX=rhs.posX;
  posY=rhs.posY;
  velX=rhs.velX;
  velY=rhs.velY;

  cur=rhs.cur;
  count = rhs.count;
  remind = rhs.remind;
  framecnt = rhs.framecnt;

  return *this;
}

void MultiSprite::draw() const {
  Uint16 w = surfs[cur]->w;
  Uint16 h = surfs[cur]->h;
  Sint16 xCoord = static_cast<Sint16>(posX);
  Sint16 yCoord = static_cast<Sint16>(posY);
  SDL_Rect src = { 0, 0, w, h };
  SDL_Rect dest = {xCoord, yCoord, 0, 0 };
  SDL_BlitSurface(surfs[cur], &src, screen, &dest);
}

void MultiSprite::draw(Sint16 x, Sint16 y) const {
  Uint16 w = surfs[cur]->w;
  Uint16 h = surfs[cur]->h;
  SDL_Rect src = { 0, 0, w, h };
  SDL_Rect dest = {x, y, 0, 0 };
  SDL_BlitSurface(surfs[cur], &src, screen, &dest);
}

void MultiSprite::update(Uint32 ticks){

  float incrX = velX * static_cast<float>(ticks) * 0.001; 
  float incrY = velY * static_cast<float>(ticks) * 0.001;
  posX+=incrX;
  posY+=incrY;

  remind -= ticks;
  if(remind<0){
    remind=count;
    cur = (cur+1)%framecnt;
    if(model&&cur==0){
      cur=framecnt-1; 
      finish = true;
    }
  }
}

float MultiSprite::getDistance(const MultiSprite& m) const{
  return hypot(posX-m.posX, posY-m.posY);
}

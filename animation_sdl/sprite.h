#include <string>
#include <SDL.h>

class Sprite {
public:
	Sprite(SDL_Surface* src, const std::string& filename, float pX, float pY, float vX, float vY, bool setColorKey=false);
	Sprite(const Sprite& s);
	~Sprite() { SDL_FreeSurface(surf); } 
	Sprite& operator=(const Sprite&);

	void draw() const;
	void draw(Sint16, Sint16) const;
	void update(Uint32 ticks);
	float X() const { return posX; }
	float Y() const { return posY; }
	float getDistance(const Sprite&) const;
	SDL_Surface* getSurf() { return surf; }

private:
	SDL_Surface* screen;
	SDL_Surface* surf;

	float posX;
	float posY;
	float velX;
	float velY;

	Sprite();
};
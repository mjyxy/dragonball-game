#include "manager.h"

Manager& Manager::getInstance() {
  static Manager manager; 
  return manager;
}

Manager::Manager() :
  screen(SDL_SetVideoMode(WIDTH, HEIGHT, 0, SDL_DOUBLEBUF)),
  genFrames(screen),
  back(screen, "images/back.bmp", 0, 0, 0, 0),
  buo(screen, "images/buo.bmp", 10, 150, 106, 64, 0, 130, 0, 0, true, 255, 1),
  boo(screen, "images/boo.bmp", 80, 125, 100, 0, true),
  line(screen, "images/line.bmp", 70, 125, 0, 0, true),
  goku(screen, "images/goku.bmp", 17, 90, 117, 75, 750, 126, 0, 0, true, 0, 1),
  boo2(screen, "images/boo2.bmp", 743, 125, -100, 0, true),
  line2(screen, "images/line.bmp", 753, 125, 0, 0, true),
  explode(screen, "images/explode.bmp", 4, 200, 99, 98, 387, 108, 0, 0, true, 0, 1),
  end(screen, "images/end.bmp", 0, 0, 0, 0, true),
  alpha(SDL_ALPHA_TRANSPARENT),
  clock(0),
  preclock(SDL_GetTicks()),
  finish(false)
  {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
      throw( std::string("Unable to initialize SDL: ")+ SDL_GetError());
    }
    if (screen == NULL) {
      throw std::string("Unable to set video mode: ")+SDL_GetError();
    }
  }

void Manager::draw() const {
  int pos=boo.X()-10;
  back.draw();
  if(buo.isDone()){
    while(pos>line.X()){
      line.draw(pos, line.Y());
      pos-=30;
    }
    line.draw();
    boo.draw();
  }
  pos=boo2.X()+10;
  if(goku.isDone()){
    while(pos<line2.X()){
      line2.draw(pos, line2.Y());
      pos+=30;
    }
    line2.draw();
    boo2.draw();
  }

  if(finish){
    explode.draw();
  } 

  buo.draw();
  goku.draw();
  if(explode.isDone()){
    end.draw();
  }

  SDL_Flip(screen);
}

void Manager::update() {
  clock = SDL_GetTicks();
  if((clock - preclock) < 17) SDL_Delay(17+preclock-clock);
  preclock+=17;

  back.update(17);
  if(buo.isDone()&& !finish){
    if(boo2.X()-30 < boo.X()) finish = true;
    else boo.update(17);
  }
  if(goku.isDone()&& !finish){
    if(boo2.X()-30 < boo.X()) finish = true;
    else boo2.update(17); 
  }

  if(finish){
    explode.update(17);
  } 
  if(explode.isDone()){
    if( alpha < SDL_ALPHA_OPAQUE ) {
      SDL_SetAlpha( end.getSurf(), SDL_SRCALPHA | SDL_RLEACCEL, ++alpha );
      alpha+=1;
    }
  }
  buo.update(17);
  goku.update(17);
}

void Manager::play() {
  SDL_Event event;
  bool done = false;
  bool makeVideo = false;
  while ( not done ) {
    while ( SDL_PollEvent(&event) ) {
      if (event.type == SDL_QUIT) done = true;
      if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym == SDLK_ESCAPE || event.key.keysym.sym == SDLK_q ) {
          done = true;
          break;
        }
        if (event.key.keysym.sym == SDLK_F4) {
          makeVideo = true;
        }
      }
    }

    draw();
    update();
    if ( makeVideo && alpha < SDL_ALPHA_OPAQUE ) {
      genFrames.makeFrame();
    }
  }
}


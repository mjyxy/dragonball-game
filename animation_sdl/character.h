#include "multisprite.h"

class Character : public MultiSprite {
public:
  Character(const std::string&, 
         const Vector2f& pos, const Vector2f& vel, const Frame*);
  // Sprite(const std::string&, const Vector2f& pos, const Vector2f& vel);

  Character(const Character& s);
  virtual ~Character() { } 
  Character& operator=(const Character&);

  // virtual const Frame* getFrame() const { return frame; }
  virtual void draw() const;

  virtual void update(Uint32 ticks);

private:
  const Frame * frame;
  int frameWidth;
  int frameHeight;
  int worldWidth;
  int worldHeight;
  int getDistance(const Sprite*) const;
};
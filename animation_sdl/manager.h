// Jingyao Ma
// This is a singleton class. I create this class to manage all sprite, multisprites
// and game time. I use SDL_delay to make sure all ticks get 17. Also all frames are generated 
// by game clock. The makevideo flag is set by F4 key_down state, and ends when the animation 
// completes.

#include <SDL.h>
#include <iostream>
#include <string>
#include "generateFrames.h"
#include "sprite.h"
#include "multisprite.h"
#include <vector>

const unsigned int WIDTH = 854u;
const unsigned int HEIGHT = 480u;

class Manager {
public:
  static Manager& getInstance();  // This class is Singleton
  void play();
private:
  SDL_Surface* const screen;
  GenerateFrames genFrames;

  Sprite back;
  MultiSprite buo;
  Sprite boo;
  Sprite line;
  MultiSprite goku;
  Sprite boo2;
  Sprite line2;
  MultiSprite explode;
  Sprite end;

  int alpha;
  int clock;
  int preclock;
  bool finish;

  void draw() const;
  void update();

  Manager ();
  ~Manager () {}
  Manager(const Manager&);
  Manager& operator=(const Manager&);
  void makeFrame();
};

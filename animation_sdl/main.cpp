// Jingyao Ma     Feb 2016    jingyam@g.clemson.edu
// This example illustrates an approach to capturing 60 frames
// per second to form a smooth, efficient animation.
// This is just a simple main function. I create a god class
// called manager and use it control this animation. 
// Manager is a singleton class, because we only need one manager.
#include "manager.h"

int main(int, char*[]) {
   try {
      Manager::getInstance().play();
   }
   catch (const std::string& msg) { std::cout << msg << std::endl; }
   catch (...) {
      std::cout << "Oops, someone threw an exception!" << std::endl;
   }
   return 0;
}
